#
# Structure de la table `PARC`
#

CREATE TABLE `PARC` (
  `id` int(11) NOT NULL auto_increment,
  `date` date NOT NULL default '0000-00-00',
  `heure` time NOT NULL default '00:00:00',
  `utilisateur` varchar(254) NOT NULL default '',
  `login` varchar(254) NOT NULL default '',
  `station` varchar(254) NOT NULL default '',
  `marque` varchar(254) NOT NULL default '',
  `modele` varchar(254) NOT NULL default '',
  `serial` varchar(254) NOT NULL default '',
  `type_pc` varchar(254) NOT NULL default '',
  `date_achat` varchar(254) NOT NULL default '',
  `ecran_marque` varchar(254) NOT NULL default '',
  `ecran_taille` varchar(254) NOT NULL default '',
  `ecran_serial` varchar(254) NOT NULL default '',
  `os` varchar(254) NOT NULL default '',
  `sp` varchar(254) NOT NULL default '',
  `processeur` varchar(254) NOT NULL default '',
  `memoire` varchar(254) NOT NULL default '',
  `disque` varchar(254) NOT NULL default '',
  `ip` varchar(254) NOT NULL default '',
  `subnet` varchar(254) NOT NULL default '',
  `gateway` varchar(254) NOT NULL default '',
  `dns1` varchar(254) NOT NULL default '',
  `dns2` varchar(254) NOT NULL default '',
  `network_drives` text,
  `network_printers` text,
  `firefox` varchar(254) NOT NULL default '',
  `ie` varchar(254) NOT NULL default '',
  `antivirus` varchar(254) NOT NULL default '',
  `antivirus_def` varchar(254) NOT NULL default '',
  `office` varchar(254) NOT NULL default '',
  `progiciels` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;

# --------------------------------------------------------

#
# Structure de la table `ARCHIVES`
#

CREATE TABLE `ARCHIVES` (
  `id` int(11) NOT NULL auto_increment,
  `date` date NOT NULL default '0000-00-00',
  `heure` time NOT NULL default '00:00:00',
  `utilisateur` varchar(254) NOT NULL default '',
  `login` varchar(254) NOT NULL default '',
  `station` varchar(254) NOT NULL default '',
  `marque` varchar(254) NOT NULL default '',
  `modele` varchar(254) NOT NULL default '',
  `serial` varchar(254) NOT NULL default '',
  `type_pc` varchar(254) NOT NULL default '',
  `date_achat` varchar(254) NOT NULL default '',
  `ecran_marque` varchar(254) NOT NULL default '',
  `ecran_taille` varchar(254) NOT NULL default '',
  `ecran_serial` varchar(254) NOT NULL default '',
  `os` varchar(254) NOT NULL default '',
  `sp` varchar(254) NOT NULL default '',
  `processeur` varchar(254) NOT NULL default '',
  `memoire` varchar(254) NOT NULL default '',
  `disque` varchar(254) NOT NULL default '',
  `ip` varchar(254) NOT NULL default '',
  `subnet` varchar(254) NOT NULL default '',
  `gateway` varchar(254) NOT NULL default '',
  `dns1` varchar(254) NOT NULL default '',
  `dns2` varchar(254) NOT NULL default '',
  `network_drives` text,
  `network_printers` text,
  `firefox` varchar(254) NOT NULL default '',
  `ie` varchar(254) NOT NULL default '',
  `antivirus` varchar(254) NOT NULL default '',
  `antivirus_def` varchar(254) NOT NULL default '',
  `office` varchar(254) NOT NULL default '',
  `progiciels` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;
    