On Error Resume Next

'Option Explicit

Function FormatiSpcMemory(intSpace) 
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace= FormatNumber(intSpace,0)
  FormatiSpcMemory = intSpace
end function

Function FormatiSpc(intSpace) 
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace= FormatNumber(intSpace,1)
  FormatiSpc = intSpace
end function

Function regValueExists (key)
      'This function checks if a registry value exists and returns True of False
      On Error Resume Next
      Dim oShell
      Set oShell = CreateObject ("WScript.Shell")
      regKeyExists = True
      Err.Clear
      oShell.RegRead(key)
      If Err <> 0 Then regValueExists = False
      Err.Clear
      Set oShell = Nothing
End Function


const HKEY_CURRENT_USER = &H80000001
Const HKEY_LOCAL_MACHINE = &H80000002
Const wbemFlagReturnImmediately = &h10
Const wbemFlagForwardOnly = &h20

Dim CPingResults

'----------[ Récupération du nom NetBIOS de la machine ]------
Set WshNetwork = WScript.CreateObject("WScript.Network")
strComputer = WshNetwork.ComputerName
strUser = WshNetwork.UserName

strServer = "CAMPUS07"
'strComputer = "."


      '----------[ Début de l'inventaire ]--------------------
	  strDay = Day(date())
	  strMonth = Month(date())
	  strYear = Year (date())
      strDate = strYear & "-" & strMonth & "-" & strDay
      strTime = time()

      '----------[ Récupération du nom détaillé de l'utilisateur et de son login dans l'AD ]----------
      Set objSysInfo = CreateObject("ADSystemInfo")
      strUser = objSysInfo.UserName
	  Set objUser = GetObject("LDAP://" & strUser)
      strDisplayName = objUser.displayName
      strUserName = objUser.sAMAccountName

      Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\CIMV2")
      Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\default:StdRegProv")


      '----------[ Récupération de la Marque et du Modèle du PC ]----------
      Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\CIMV2")
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_ComputerSystem")
      For Each objItem In colItems
        strMarque = objItem.Manufacturer
        strModele = objItem.Model
      Next

      '----------[ Récupération du Service Tag DELL ]

      Set objWMIService = GetObject("winmgmts:" & _
		"{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
      For Each objSMBIOS in objWMIService.ExecQuery("Select * from Win32_SystemEnclosure") 
	    strServiceTag = objSMBIOS.SerialNumber 
      Next


      '----------[ Récupération de la version Microsoft Windows + Service Pack ]----------
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      For Each objItem In colItems
        strOS = objItem.Caption
        strSP = objItem.CSDVersion
      Next

      '----------[ Récupération du type de processeur ]----------
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_Processor", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      Count = 0
      For Each objItem In colItems
        Count = Count + 1
        strProcesseur = Trim(objItem.Name)
      Next
      If Count = 1 Then
        Reponse = Reponse
      End If

      '----------[ Récupération de la taille de la mémoire ]----------
      Set colComputer = objWMIService.ExecQuery ("SELECT * from Win32_ComputerSystem", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      For Each objComputer in colComputer
        strMemoire = FormatiSpcMemory(objComputer.TotalPhysicalMemory)
      Next

      '----------[ Récupération de l'espace disque C et D (total + libre en Go)]----------
      '----------[ Disque C: ]----------
      Set colItems = objWMIService.ExecQuery("Select * From Win32_LogicalDisk Where (DriveType = 3) And (DeviceID='C:')")
      For Each objItem In colItems
        If IsNull(objItem.DeviceID) Then
          Reponse = Reponse & ";;"
        Else
          iSpcFree = cDbl(objItem.FreeSpace)
          iSpcTotal = cDbl(objItem.Size)
          strDisque = objItem.DeviceID & " " & FormatiSpc(iSpcTotal)
          'Reponse = Reponse & objItem.DeviceID & " " & FormatiSpc(iSpcTotal) & ";"
          'Reponse = Reponse & objItem.DeviceID & " " & FormatiSpc(iSpcFree) & ";"
        End If
      Next


      '----------[ Récupération des paramètres TCP/IP de toutes les cartes réseaux actives ]----------
      Set cItems = objWMIService.ExecQuery("Select * from Win32_NetworkAdapterConfiguration",,48)
      For Each objItem in cItems
        For Each propValue in objItem.IPAddress
          strIP = trim(objItem.IPAddress(0))
          'MyMac= objItem.MACAddress(0)
          strSubnet = trim(objItem.IPSubnet(0))
          strGateway = trim(objItem.DefaultIPGateway(0))
          strDNS1 = trim(objItem.DNSServerSearchOrder(0))
          strDNS2 = trim(objItem.DNSServerSearchOrder(1))
        Next
        'Reponse = Reponse & MyIP & ";" & MySubnet & ";" & MyGateway & ";" & MyDNS1 & ";" & MyDNS2 & ";"
      Next

	  
	  '----------[ Récupération des lecteurs réseaux ]----------
      Set objWMIService = GetObject("winmgmts:" _
          & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

      Set colItems = objWMIService.ExecQuery("Select * from Win32_MappedLogicalDisk")
      strNetworkDrives = ""
      For Each objItem in colItems
              strNetworkDrives = strNetworkDrives & objItem.Name & " " & objItem.ProviderName & "<BR>"
	      'Reponse = Reponse & objItem.Name & " " & objItem.ProviderName & "<BR>"
      Next
      'Reponse = Reponse & ";"	  

'----------[ Récupération des imprimantes installées ]---------------
Const ForAppending = 8 
Const ForReading = 1 
Dim WshNetwork, objPrinter, intDrive, intNetLetter
'strComputer = "."
Set WshNetwork = CreateObject("WScript.Network") 
Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
Set colInstalledPrinters = objWMIService.ExecQuery("Select * from Win32_Printer") 
Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48) 
Set WshShell = WScript.CreateObject("WScript.Shell") 

Set objOutputFile = objFSO.OpenTextFile (filOutput, ForAppending, True) 
strNetworkPrinters = ""
For Each objPrinter in colInstalledPrinters 
strTest = Left(objPrinter.Name, 2)
strNetworkPrinters = strNetworkPrinters & objPrinter.Name & "<BR>" 
'Reponse = Reponse & objPrinter.Name & "<BR>"
Next 

Set objPrinter = WshNetwork.EnumPrinterConnections
If objPrinter.Count = 0 Then
strNetworkPrinters = strNetworkPrinters
else
For intDrive = 0 To (objPrinter.Count -1) Step 2
intNetLetter = IntNetLetter +1
strNetworkPrinters = strNetworkPrinters & "\\" & objPrinter.Item(intDrive) & " = " & objPrinter.Item(intDrive +1) & "<BR>"
'Reponse = Reponse & "\\" & objPrinter.Item(intDrive) & " = " & objPrinter.Item(intDrive +1) & "<BR>"
Next
end if
'Reponse = Reponse & ";"
'--------------------------------------------------------------------------------------	  

'Find the Firefox version (64 Bit machines with 32-bit Firefox installed)
Set objFSO = CreateObject("Scripting.FileSystemObject")
If (fso.FileExists("C:\Program Files (x86)\Mozilla Firefox\firefox.exe")) Then
file = "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
strFirefox = "Firefox " & objFSO.GetFileVersion(file)
'Reponse = Reponse & "Firefox " & objFSO.GetFileVersion(file) & ";"
else

If (fso.FileExists("C:\Program Files\Mozilla Firefox\firefox.exe")) Then
file = "C:\Program Files\Mozilla Firefox\firefox.exe"
strFirefox = "Firefox " & objFSO.GetFileVersion(file)
'Reponse = Reponse & "Firefox " & objFSO.GetFileVersion(file) & ";"
else
strFirefox = ""
'Reponse = Reponse & ";"
end if
end if
 
'IE Version
file = "C:\Program Files\Internet Explorer\iexplore.exe"
strIE = "IE " & objFSO.GetFileVersion(file)
'Reponse = Reponse & "IE " & objFSO.GetFileVersion(file) & ";"

'----------[ Vérification de Norton Antivirus et récupération de la date de définition de virus ] ----------
'strComputer = "."
Set objRegistry = GetObject("winmgmts:\\" & strComputer & "\root\default:StdRegProv")
strKeyPath = "Software\Wow6432Node\Symantec\SharedDefs"
strValueName = "DEFWATCH_10"
objRegistry.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
If IsNull(strValue) Then

    strAntivirus = ""
    strAntivirusDef = ""
    'Reponse = Reponse & ";" & ";"

Else

Dim WshShell, bKey, bits, valueDefVer, yearDef, monthDef, dayDef, revDef, DefinitionVersion
Set WshShell = WScript.CreateObject("WScript.Shell")
bKey = WshShell.RegRead("HKLM\Software\Wow6432Node\Symantec\SharedDefs\DEFWATCH_10")
bits = Split(bKey, "\")
valueDefVer  = bits(UBound(bits))
yearDef = Left(valueDefVer, 4)
monthDef = Mid(valueDefVer, 5, 2)
dayDef = Mid(valueDefVer, 7, 2)
revDef = Right(valueDefVer, 3)
If Left(revDef, 1) = 0 Then
 revDef = Right(revDef, 2)
 strAntivirus = "Norton Antivirus"
 strAntivirusDef = dayDef & "/" & monthDef & "/" & yearDef & " rev." & revDef
 'Reponse = Reponse & "Norton Antivirus" & ";" & monthDef & "/" & dayDef & "/" & yearDef & " rev." & revDef & ";"
Else
 strAntivirus = "Norton Antivirus"
 strAntivirusDef = dayDef & "/" & monthDef & "/" & yearDef & " rev." & revDef
 'Reponse = Reponse & "Norton Antivirus" & ";" & monthDef & "/" & dayDef & "/" & yearDef & " rev." & revDef & ";"
End If

End If
	  

      '----------[ Récupération de la version de Microsoft Office ]----------
      Set colSoft = objWMIService.ExecQuery("SELECT * FROM Win32_Product WHERE Name Like 'Microsoft Office%'")

      If colSoft.Count = 0 Then
        strOffice = ""
        'Reponse = Reponse & ";"
      else
         For Each objItem In colSoft
            strOffice = objitem.caption
            'Reponse = Reponse & objitem.caption & ";"
            exit for
         Next
      End If


	  
'''Wscript.Echo strComputer & " " & strDate & " " & strTime
      '-----------[ Ecritre des infos dans une base MySQL]------------------------------
Set oCn = CreateObject("ADODB.Connection")
Set oRs = CreateObject("ADODB.Recordset")

ConnectionString = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=10.0.0.9;" & _
   "DATABASE=inventorix;" & _
   "USER=root;" & _
   "PASSWORD=informatique;" & _
   "OPTION=11;"

oCn.open(ConnectionString)
 'Open your connection

    Dim strSQLcheck
    strSQLcheck = "SELECT * FROM parc WHERE station = '" & strComputer & "';"
	Dim TEST
    oRs.Open strSQLcheck, oCn
'''Wscript.Echo oRs.EOF

    if oRs.EOF Then    
    '-----> [ le nom de PC n'existe pas dans la base / On ajoute une ligne ]
    Dim strSQLadd
    strSQLadd = "INSERT INTO parc (date, heure, utilisateur, login, station, marque, modele, serial, os, sp, processeur, memoire, disque, ip, subnet, gateway, dns1, dns2, network_drives, network_printers, firefox, ie, antivirus, antivirus_def, office)" & _
    " VALUES ('" & strDate & "','" & strTime & "','" & strDisplayName & "','" & strUserName & "','" & strComputer & "','" & strMarque & "','" & strModele & "','" & strServiceTag & "','" & strOS & "','" & strSP & "','" & strProcesseur & "','" & strMemoire & "','" & strDisque & "','" & strIP & "','" & strSubnet & "','" & strGateway & "','" & strDNS1 & "','" & strDNS2 & "','" & strNetworkDrives & "','" & strNetworkPrinters & "','" & strFirefox & "','" & strIE & "','" & strAntivirus & "','" & strAntivirusDef & "','" & strOffice & "');"
    oCn.Execute strSQLadd
    Else
    '-----> [ le nom de PC existe dans la base / On modifie la ligne ]
    Dim strSQLupdate
    strSQLupdate= "UPDATE parc SET date='" & strDate & "', heure='" & strTime & "', utilisateur='" & strDisplayName & "', login='" & strUserName & "', marque='" & strMarque & "', modele='" & strModele & "', serial='" & strServiceTag & "', os='" & strOS & "', sp='" & strSP & "', processeur='" & strProcesseur & "', memoire='" & strMemoire & "', disque='" & strDisque & "', ip='" & strIP & "', subnet='" & strSubnet & "', gateway='" & strGateway & "', dns1='" & strDNS1 & "', dns2='" & strDNS2 & "', network_drives='" & strNetworkDrives & "', network_printers='" & strNetworkPrinters & "', firefox='" & strFirefox & "', ie='" & strIE & "', antivirus='" & strAntivirus & "', antivirus_def='" & strAntivirusDef & "', office='" & strOffice & "' WHERE station='" & strComputer & "';"
    oCn.Execute strSQLupdate   
    End if
    'Inser the field

 oRs.close()
 oCn.close()
 'Close all connections
	  
'----------------------------------
'----------------------------------