On Error Resume Next

'Option Explicit

Function FormatiSpcMemory(intSpace) 
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace= FormatNumber(intSpace,0)
  FormatiSpcMemory = intSpace
end function

Function FormatiSpc(intSpace) 
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace = intSpace/1024
  intSpace= FormatNumber(intSpace,1)
  FormatiSpc = intSpace
end function

Function regValueExists (key)
      'This function checks if a registry value exists and returns True of False
      On Error Resume Next
      Dim oShell
      Set oShell = CreateObject ("WScript.Shell")
      regKeyExists = True
      Err.Clear
      oShell.RegRead(key)
      If Err <> 0 Then regValueExists = False
      Err.Clear
      Set oShell = Nothing
End Function


const HKEY_CURRENT_USER = &H80000001
Const HKEY_LOCAL_MACHINE = &H80000002
Const wbemFlagReturnImmediately = &h10
Const wbemFlagForwardOnly = &h20

Dim CPingResults

'----------[ Récupération du nom NetBIOS de la machine ]------
Set WshNetwork = WScript.CreateObject("WScript.Network")
strComputer = WshNetwork.ComputerName
strUser = WshNetwork.UserName

strServer = "CAMPUS07"
'strComputer = "."


      '----------[ Début de l'inventaire ]--------------------
      Reponse = date() & ";" & time() & ";"

      '----------[ Récupération du nom détaillé de l'utilisateur et de son login dans l'AD ]----------
      Set objSysInfo = CreateObject("ADSystemInfo")
      strUser = objSysInfo.UserName
	  Set objUser = GetObject("LDAP://" & strUser)
      DisplayName = objUser.displayName
	  UserName = objUser.sAMAccountName	  
      Reponse = Reponse & DisplayName & ";" & UserName & ";"


      Reponse = Reponse & strComputer & ";"


      Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\CIMV2")
      Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\default:StdRegProv")

      '----------[ Récupération de la Marque et du Modèle du PC ]----------
      Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\CIMV2")
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_ComputerSystem")
      For Each objItem In colItems
	Reponse = Reponse & objItem.Manufacturer & ";" & objItem.Model & ";"
      Next

      '----------[ Récupération du Service Tag DELL ]

      Set objWMIService = GetObject("winmgmts:" & _
		"{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
      For Each objSMBIOS in objWMIService.ExecQuery("Select * from Win32_SystemEnclosure") 
	    ServiceTag = objSMBIOS.SerialNumber 
      Next
      Reponse = Reponse & ServiceTag & ";"


      '----------[ Récupération de la version Microsoft Windows + Service Pack ]----------
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      For Each objItem In colItems
        Reponse = Reponse & objItem.Caption & ";" & objItem.CSDVersion & ";"
      Next

      '----------[ Récupération du type de processeur ]----------
      Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_Processor", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      Count = 0
      For Each objItem In colItems
        Count = Count + 1
        Reponse = Reponse & Trim(objItem.Name) & ";"
      Next
      If Count = 1 Then
        Reponse = Reponse
      End If

      '----------[ Récupération de la taille de la mémoire ]----------
      Set colComputer = objWMIService.ExecQuery ("SELECT * from Win32_ComputerSystem", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
      For Each objComputer in colComputer
        Reponse = Reponse & FormatiSpcMemory(objComputer.TotalPhysicalMemory) & ";"
      Next

      '----------[ Récupération de l'espace disque C et D (total + libre en Go)]----------
      '----------[ Disque C: ]----------
      Set colItems = objWMIService.ExecQuery("Select * From Win32_LogicalDisk Where (DriveType = 3) And (DeviceID='C:')")
      For Each objItem In colItems
        If IsNull(objItem.DeviceID) Then
          Reponse = Reponse & ";;"
        Else
          iSpcFree = cDbl(objItem.FreeSpace)
          iSpcTotal = cDbl(objItem.Size)
          Reponse = Reponse & objItem.DeviceID & " " & FormatiSpc(iSpcTotal) & ";"
          'Reponse = Reponse & objItem.DeviceID & " " & FormatiSpc(iSpcFree) & ";"
        End If
      Next


      '----------[ Récupération des paramètres TCP/IP de toutes les cartes réseaux actives ]----------
      Set cItems = objWMIService.ExecQuery("Select * from Win32_NetworkAdapterConfiguration",,48)
      For Each objItem in cItems
        For Each propValue in objItem.IPAddress
          MyIP = objItem.IPAddress(0)
          'MyMac= objItem.MACAddress(0)
          MySubnet = objItem.IPSubnet(0)
          MyGateway = objItem.DefaultIPGateway(0)
          MyDNS1 = objItem.DNSServerSearchOrder(0)
          MyDNS2 = objItem.DNSServerSearchOrder(1)
        Next
        Reponse = Reponse & MyIP & ";" & MySubnet & ";" & MyGateway & ";" & MyDNS1 & ";" & MyDNS2 & ";"
      Next

	  
	  '----------[ Récupération des lecteurs réseaux ]----------
      Set objWMIService = GetObject("winmgmts:" _
          & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

      Set colItems = objWMIService.ExecQuery("Select * from Win32_MappedLogicalDisk")

      For Each objItem in colItems
	      Reponse = Reponse & objItem.Name & " " & objItem.ProviderName & "<BR>"
      Next
      Reponse = Reponse & ";"	  

'----------[ Récupération des imprimantes installées ]---------------
Const ForAppending = 8 
Const ForReading = 1 
Dim WshNetwork, objPrinter, intDrive, intNetLetter
strComputer = "."
Set WshNetwork = CreateObject("WScript.Network") 
Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
Set colInstalledPrinters = objWMIService.ExecQuery("Select * from Win32_Printer") 
Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48) 
Set WshShell = WScript.CreateObject("WScript.Shell") 

Set objOutputFile = objFSO.OpenTextFile (filOutput, ForAppending, True) 
For Each objPrinter in colInstalledPrinters 
strTest = Left(objPrinter.Name, 2) 
Reponse = Reponse & objPrinter.Name & "<BR>"
Next 

Set objPrinter = WshNetwork.EnumPrinterConnections
If objPrinter.Count = 0 Then
Reponse = Reponse
else
For intDrive = 0 To (objPrinter.Count -1) Step 2
intNetLetter = IntNetLetter +1
Reponse = Reponse & "\\" & objPrinter.Item(intDrive) & " = " & objPrinter.Item(intDrive +1) & "<BR>"
Next
end if
Reponse = Reponse & ";"
'--------------------------------------------------------------------------------------	  

'Find the Firefox version (64 Bit machines with 32-bit Firefox installed)
Set objFSO = CreateObject("Scripting.FileSystemObject")
If (fso.FileExists("C:\Program Files (x86)\Mozilla Firefox\firefox.exe")) Then
file = "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
Reponse = Reponse & "Firefox " & objFSO.GetFileVersion(file) & ";"
else

If (fso.FileExists("C:\Program Files\Mozilla Firefox\firefox.exe")) Then
file = "C:\Program Files\Mozilla Firefox\firefox.exe"
Reponse = Reponse & "Firefox " & objFSO.GetFileVersion(file) & ";"
else
Reponse = Reponse & ";"
end if
end if
 
'IE Version
file = "C:\Program Files\Internet Explorer\iexplore.exe"
Reponse = Reponse & "IE " & objFSO.GetFileVersion(file) & ";"

'----------[ Vérification de Norton Antivirus et récupération de la date de définition de virus ] ----------
strComputer = "."
Set objRegistry = GetObject("winmgmts:\\" & strComputer & "\root\default:StdRegProv")
strKeyPath = "Software\Wow6432Node\Symantec\SharedDefs"
strValueName = "DEFWATCH_10"
objRegistry.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
If IsNull(strValue) Then

    Reponse = Reponse & ";" & ";"

Else

Dim WshShell, bKey, bits, valueDefVer, yearDef, monthDef, dayDef, revDef, DefinitionVersion
Set WshShell = WScript.CreateObject("WScript.Shell")
bKey = WshShell.RegRead("HKLM\Software\Wow6432Node\Symantec\SharedDefs\DEFWATCH_10")
bits = Split(bKey, "\")
valueDefVer  = bits(UBound(bits))
yearDef = Left(valueDefVer, 4)
monthDef = Mid(valueDefVer, 5, 2)
dayDef = Mid(valueDefVer, 7, 2)
revDef = Right(valueDefVer, 3)
If Left(revDef, 1) = 0 Then
 revDef = Right(revDef, 2)
 Reponse = Reponse & "Norton Antivirus" & ";" & monthDef & "/" & dayDef & "/" & yearDef & " rev." & revDef & ";"
Else
 Reponse = Reponse & "Norton Antivirus" & ";" & monthDef & "/" & dayDef & "/" & yearDef & " rev." & revDef & ";"
End If

End If
	  

      '----------[ Récupération de la version de Microsoft Office ]----------
      Set colSoft = objWMIService.ExecQuery("SELECT * FROM Win32_Product WHERE Name Like 'Microsoft Office%'")

      If colSoft.Count = 0 Then
        Reponse = Reponse & ";"
      else
         For Each objItem In colSoft
            Reponse = Reponse & objitem.caption & ";"
            exit for
         Next
      End If



      '-----------[ Ecritre des infos dans un fichier texte ]------------------------------
      Set MyShell = CreateObject("Wscript.Shell")
      Set fso = Wscript.CreateObject("Scripting.FilesystemObject")
      LogFileName= "\\CAMPUS01\INVENTORIX\campus93-inventorix.txt"
      
      
      Set fsHandle = fso.OpenTextFile (LogFileName,8,True)

      fsHandle.Writeline Reponse
      fsHandle.close
      Set MyShell = Nothing
      Set fso = Nothing