'=========================================================================='
' Title: List Printers.vbs' 
' Date: 02/23/2010' 
' Author: Bradley Buskey' 
' Version: 1.00' 
' Updated: 02/23/2010' 
' Purpose: List all printers attached to a workstation' 
'=========================================================================='
'=========================================================================='
' Date 03/29/2010
' updated: Chris Daws
' to include mapped network printers
'----------[ Récupération des imprimantes installées ]---------------
Const ForAppending = 8 
Const ForReading = 1 
Dim WshNetwork, objPrinter, intDrive, intNetLetter
strComputer = "."
Set WshNetwork = CreateObject("WScript.Network") 
Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
Set colInstalledPrinters = objWMIService.ExecQuery("Select * from Win32_Printer") 
Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48) 
Set WshShell = WScript.CreateObject("WScript.Shell") 

Set objOutputFile = objFSO.OpenTextFile (filOutput, ForAppending, True) 
For Each objPrinter in colInstalledPrinters 
strTest = Left(objPrinter.Name, 2) 
Reponse = Reponse & objPrinter.Name & "<BR>"
Next 

Set objPrinter = WshNetwork.EnumPrinterConnections
If objPrinter.Count = 0 Then
Reponse = Reponse
else
For intDrive = 0 To (objPrinter.Count -1) Step 2
intNetLetter = IntNetLetter +1
Reponse = Reponse "\\" & objPrinter.Item(intDrive) & " = " & objPrinter.Item(intDrive +1) & "<BR>"
Next
end if
Reponse = Reponse & ";"
'--------------------------------------------------------------------------------------

