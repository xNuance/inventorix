<?php require_once('connexion.php'); ?>

<?php 
// ------ CONNEXION A LA BASE DE DONNEES PARC --------
$user="root";
$host="localhost";
$password="informatique";
$database="INVENTORIX";

$connection = new mysqli($host,$user,$password,$database);


$resultuser = $connection->query ("SELECT DISTINCT utilisateur FROM parc ORDER BY utilisateur");
$row_user = mysqli_fetch_assoc($resultuser);


$resultos = $connection->query ("SELECT DISTINCT os FROM parc ORDER BY os");
$row_os = mysqli_fetch_assoc($resultos);
$rowsos = mysqli_num_rows($resultos); // pour le graphique (plus bas dans la partie JAVASCRIPT)

$resultmodele = $connection->query ("SELECT DISTINCT modele FROM parc ORDER BY modele");
$row_modele = mysqli_fetch_assoc($resultmodele);

@$add= $_REQUEST['add']; // On récupere l'éventuel ajout de champ(s) ...
if($add=="")
{@$add= $_GET['add'];}

 // On récupere l'état des boutons d'ajout de champs ...
@$checkp= $_REQUEST['checkp'];
if($checkp=="")
{@$checkp= $_GET['checkp'];}

@$checkl= $_REQUEST['checkl'];
if($checkl=="")
{@$checkl= $_GET['checkl'];}

@$checku= $_REQUEST['checku'];
if($checku=="")
{@$checku= $_GET['checku'];}

@$checkv= $_REQUEST['checkv'];
if($checkv=="")
{@$checkv= $_GET['checkv'];}

// AFFICHAGE DES CHAMPS SUPPLEMENTAIRES ...
if(isset($_POST['champs'])){ // on vérifie la présence des variables de formulaire (si le formulaire a été envoyé)
   if (($_REQUEST['processeur'] != "proc") && ($_REQUEST['login2'] != "login") && ($_REQUEST['utilisateur2'] != "user") && ($_REQUEST['vnc'] != "vnc")) {
      @$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
      if($commande=="")
      {@$commande= $_GET['commande'];}
      header("Location: index.php?commande=$commande");}
   else
   {
   $processeur = $_REQUEST['processeur'];
   $login2 = $_REQUEST['login2'];
   $utilisateur2 = $_REQUEST['utilisateur2'];
   $vnc = $_REQUEST['vnc'];
   
   @$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
   if($commande=="")
   {@$commande= $_GET['commande'];}

   // On récupere le numéro de la page actuelle consultée ...
   @$addpage= $_REQUEST['page'];
   if($addpage=="")
   {@$addpage= $_GET['page'];}
   
   @$addstart= $_REQUEST['start'];
   if($addstart=="")
   {@$addstart= $_GET['start'];}
   
   if($processeur == "proc") {$addproc = "p";$checkp = 1;} else {$addproc = "";$checkp = 0;}
   if($login2 == "login") {$addlogin = "l";$checkl = 1;} else {$addlogin = "";$checkl = 0;}
   if($utilisateur2 == "user") {$adduser = "u";$checku = 1;} else {$adduser = "";$checku = 0;}
   if($vnc == "vnc") {$addvnc = "v";$checkv = 1;} else {$addvnc = "";$checkv = 0;}
   $add = $addproc.$addlogin.$adduser.$addvnc;
   
   header("Location: index.php?add=$add&commande=$commande&page=$addpage&start=$addstart&checkp=$checkp&checkl=$checkl&checku=$checku&checkv=$checkv");
   }
   }
    
// AFFICHAGE DE LA RECHERCHE RAPIDE DANS LA BASE ACTUELLE
if(isset($_POST['station'])){ // on vérifie la présence des variables de formulaire (si le formulaire a été envoyé)
   if (($_REQUEST['station'] == "") && ($_REQUEST['tcpip'] == "") && ($_REQUEST['modele'] == "") && ($_REQUEST['user'] == "Faites votre choix dans la liste ...") && ($_REQUEST['os'] == "Faites votre choix dans la liste ...")) {
      header("Location: index.php?affiche=empty");}
   else
   {
   
   $station = $_REQUEST['station'];
   $tcpip = $_REQUEST['tcpip'];
   $modele = $_REQUEST['modele'];
   $user = $_REQUEST['user'];
   $os = $_REQUEST['os'];
   
   $premier = "oui";
    $sql = "SELECT * FROM parc WHERE ";
    
    
    if($station != "")
    {
       if($premier=="non")
       {$sql = $sql . " AND ";}
       else
       {$premier="non";}

       $sql = $sql . "station LIKE '%".$station."%'";
    }
	    
    if($tcpip !="")
    {
       if($premier=="non")
       {$sql = $sql . " AND ";}
       else
       {$premier="non";}

       $sql = $sql . "ip LIKE '%".$tcpip."%'";
    }

    if($modele !="Faites votre choix dans la liste ...")
    {
       if($premier=="non")
       {$sql = $sql . " AND ";}
       else
       {$premier="non";}

       $sql = $sql . "modele = '".$modele."'";
    }
	
    if($user !="Faites votre choix dans la liste ...")
    {
       if($premier=="non")
       {$sql = $sql . " AND ";}
       else
       {$premier="non";}

       $sql = $sql . "utilisateur = '".$user."'";
    }
	
	
    if($os !="Faites votre choix dans la liste ...")
    {
       if($premier=="non")
       {$sql = $sql . " AND ";}
       else
       {$premier="non";}
       
       $sql = $sql . "os = '".$os."'";
    }

    $sql = $sql . " ORDER BY station";
	// envoi de la requete ...
	header("Location: index.php?commande=$sql");
	}
}

?>


<html>
<head>
    <link rel="stylesheet" media="all" type="text/css" href="css/style.css" />
    <title>INVENTORIX - Gestion de Parc Informatique : Liste des postes de travail en temps r&eacute;el</title>
<SCRIPT language=Javascript1.2>
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</SCRIPT>


		<script src="js/Chart.js"></script>
		<meta name = "viewport" content = "initial-scale = 1, user-scalable = no">
</head>
<body>
    <div class="site">
    <div class="site2">
        <div class="moteur_recherche">
            <form action="" method="post" name="search">
                <div>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="search_box">
                    <tr>
                      <td class="padding2" width="100" height="25" align="left" valign="middle">Station :</td>
                      <td colspan="2" align="left" valign="middle"><input name="station" type="text" id="station" size="20" /></td>
                    </tr>
                    <tr>
                      <td class="padding2" width="100" height="25" align="left" valign="middle">Adresse IP :</td>
                      <td colspan="2" align="left" valign="middle"><input name="tcpip" type="text" id="tcpip" size="20" /></td>
                    </tr>
                    <tr>
                      <td class="padding2" width="100" height="25" align="left" valign="middle">Utilisateur :</td>
                      <td colspan="2" align="left" valign="middle"><select name="user" class="textform" id="select15">
                        <option selected="selected">Faites votre choix dans la liste ...</option>
                        <?php
do {  
?>
                        <option value="<?php echo $row_user['utilisateur']?>">
                          <?php 
				echo $row_user['utilisateur'];
					?>
                        </option>
                        <?php
} while ($row_user = mysqli_fetch_assoc($resultuser));
  $rowsuser = mysqli_num_rows($resultuser);
  if($rowsuser > 0) {
      mysqli_data_seek($resultuser, 0);
	  $row_user = mysqli_fetch_assoc($resultuser);
  }
?>
                      </select></td>
                    </tr>
                    <tr>
                      <td class="padding2" width="100" height="25" align="left" valign="middle">Mod&egrave;le PC :</td>
                      <td width="150" align="left" valign="middle">
<select name="modele" class="textform" id="select2">
                          <option selected="selected">Faites votre choix dans la liste ...</option>
                          <?php
do {  
?>
                          <option value="<?php echo $row_modele['modele']?>">
                            <?php
							$resultmarque = $connection->query ("SELECT DISTINCT marque FROM parc WHERE modele='".$row_modele['modele']."' ORDER BY modele");
                            $row_marque = mysqli_fetch_assoc($resultmarque);
				echo $row_marque['marque'].' '.$row_modele['modele'];
					?>
                          </option>
                          <?php
} while ($row_modele = mysqli_fetch_assoc($resultmodele));
  $rowsmodele = mysqli_num_rows($resultmodele);
  if($rowsmodele > 0) {
      mysqli_data_seek($resultmodele, 0);
	  $row_modele = mysqli_fetch_assoc($resultmodele);
  }
?>
</select>
                      </td>
                      <td align="left" valign="middle">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="padding2" width="100" height="25" align="left" valign="middle">Syst&egrave;me OS :</td>
                      <td colspan="2" align="left" valign="middle"><strong><font size="2" face="Arial, Helvetica, sans-serif">
                        <select name="os" class="textform" id="select">
                          <option selected="selected">Faites votre choix dans la liste ...</option>
                          <?php
do {  
?>
                          <option value="<?php echo $row_os['os']?>">
                            <?php 
				echo $row_os['os'];
					?>
                          </option>
                          <?php
} while ($row_os = mysqli_fetch_assoc($resultos));
  $rowsos = mysqli_num_rows($resultos);
  if($rowsos > 0) {
      mysqli_data_seek($resultos, 0);
	  $row_os = mysqli_fetch_assoc($resultos);
  }
?>
                        </select>
                      </font></strong></td>
                    </tr>
                    <tr>
                      <td height="28" colspan="3" align="center" valign="bottom"><input name="Submit1" type="submit" id="Submit1" value="Rechercher dans la base" /></td>
                    </tr>
                  </table>
                </div>
            </form>
		</div>
        <div class="header">
	        Inventorix
        </div>
        <div align="center" class="clean"><strong><em>Pr&ecirc;t &aacute; l'emploi! Gestion de parc informatique automatis&eacute;e par script VBS-to-MySQL via GPO dans un domaine Microsoft</em></strong></div>
        <div class="corp">
            <div class="corp_ctn">
                            <h1>Parc informatique de la CMA et du CFA 93</h1>
                <div class="paragraphe">
                  <?php if(isset($_GET['commande']) && ($_GET['commande'] != "")) { // Affiche l'erreur
	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
    echo '<b>'.$total.' Postes recens&eacute;s'.'</b>';
} else {
	
	$commande = "SELECT * FROM parc ORDER BY station";
    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
    echo '<b>'.$total.' Postes recens&eacute;s'.'</b>';
}
?>
                  <?php if(isset($_GET['commande']) && ($_GET['commande'] != "SELECT * FROM parc ORDER BY station")) { // Affiche l'erreur
    echo '<a href="index.php?commande=SELECT%20*%20FROM%20parc%20ORDER%20BY%20station" STYLE="text-decoration: none;color: white">[Cliquez ici pour afficher l\'int&eacute;gralit&eacute; du parc]</a>';
} ?>
                <br />
                    <br />
<h2>Liste des stations recens&eacute;es automatiquement : 
  
  <?php 	
	@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;}

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}
	
	if(!$commande) {$commande ="SELECT * FROM parc ORDER BY station";}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}

    if($start>0){
	if(isset($_GET['add']) && ($_GET['add'] != "")) { 
    echo("<a href=\"index.php?add=".($add)."&commande=$commande&page=".($page-1)."&start=".($start-50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\"> << </a>");
	}
	else {
	echo("<a href=\"index.php?commande=$commande&page=".($page-1)."&start=".($start-50)."\"> << </a>");
	}}
 
	?>
  <?php  
		  	@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;}

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}
	
	if(!$commande) {$commande ="SELECT * FROM parc ORDER BY station";}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
	
	
	echo '<b>'.$page."/".ceil($total/50).'</b>';
		  ?>
  <?php 	
	@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;}

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}
	
	if(!$commande) {$commande ="SELECT * FROM parc ORDER BY station";}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
    
	if($total>$start+50){
    if(isset($_GET['add']) && ($_GET['add'] != "")) {
	echo("<a href=\"index.php?add=".($add)."&commande=$commande&page=".($page+1)."&start=".($start+50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\"> >> </a>");
	}
	else { 
    echo("<a href=\"index.php?commande=$commande&page=".($page+1)."&start=".($start+50)."\"> >> </a>");
	}}  
	?>
</h2>

                    <div align="center" class="paragraphe">
                      <?php if(isset($_GET['commande']) && ($_GET['commande'] != "")) { // Affiche l'erreur ?>
                      <?php
	
	@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;} 

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
	
	$select = $commande." LIMIT $start,50";
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
// SUITE ...
// si on a récupéré un résultat on l'affiche.
if($total) {
    // début du tableau
    echo '<table width="620" class="ligne_box" border="0" cellspacing="0" cellpadding="0">'."\n";
        // première ligne on affiche les titres
        echo '<tr>';
        echo '<td width="90" bgcolor="#666666"><b><font color="#FFFFFF">NOM [STATION]</font></b></td>';
		echo '<td width="60" bgcolor="#666666"><b><font color="#FFFFFF">MARQUE</font></b></td>';
		echo '<td width="90" bgcolor="#666666"><b><font color="#FFFFFF">MODELE</font></b></td>';
		echo '<td width="80" bgcolor="#666666"><b><font color="#FFFFFF">N&deg; DE SERIE</font></b></td>';
        echo '<td width="75" bgcolor="#666666"><b><font color="#FFFFFF">ADRESSE IP</font></b></td>';
		echo '<td width="80" align="center" bgcolor="#666666"><b><font color="#FFFFFF">ANTIVIRUS</font></b></td>';
		echo '<td width="145" bgcolor="#666666"><b><font color="#FFFFFF">UTILISATEUR [LOGIN]</font></b></td>';
        echo '</tr>'."\n";
    // lecture et affichage des résultats, 1 résultat par ligne.    
    while($row = mysqli_fetch_array($result)) {
        echo '<tr onMouseOver="style.backgroundColor=\'#FFFFFF\'" onMouseOut="style.backgroundColor=\'\'" style="border-bottom: 1px solid #CACACA">';
        echo '<td class="padding2" width="90" height="30" style="border-bottom: 1px solid #CACACA"><font size="1"><b>'.("<a onclick=\"MM_openBrWindow('fichepc.php?infos=sessions&commande=".($row['station'])."', 'FICHEPC', 'width=600,height=550,top=50,left=50')\" STYLE=\"text-decoration: none;color: blue;cursor:pointer;\">").$row['station'].'</a></b></font></td>';
		echo '<td width="60" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['marque'].'</font></td>';
		echo '<td width="90" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['modele'].'</font></td>';
		echo '<td width="80" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['serial'].'</font></td>';
        echo '<td width="75" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['ip'].'</font></td>';
		if ($row['antivirus']=="") {echo '<td width="80" height="30" align="center" style="border-bottom: 1px solid #CACACA"><img src="Icones/checkR.png" alt=""/></td>';}
		else {echo '<td width="80" height="30" align="center" style="border-bottom: 1px solid #CACACA"><img src="Icones/checkV.png" alt=""/></td>';}
		echo '<td width="145" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['login'].'</font></td>';
        echo '</tr>'."\n";
    }
        echo '<tr>';
        echo '<td width="90">&nbsp;</td>';
		echo '<td width="60">&nbsp;</td>';
		echo '<td width="90">&nbsp;</td>';
		echo '<td width="80">&nbsp;</td>';
        echo '<td width="75">&nbsp;</td>';
		echo '<td width="80">&nbsp;</td>';
		echo '<td width="145">&nbsp;</td>';
        echo '</tr>'."\n";
    echo '</table>'."\n";
    // fin du tableau.
}
else echo '<font color="#3366CC"><b>Aucun enregistrement ne correspond à votre requête...</b></font>';

// on libère le résultat
mysqli_free_result($result);

@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;} 

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
	
	//Affichage "Page précèdente" 
    if($start>0){ 
    echo("<a href=\"index.php?commande=$commande&page=".($page-1)."&start=".($start-50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\">Page pr&eacute;c&eacute;dente</a>");}
    //Affichage "Page suivante" 
    if($total>$start+50){ 
    echo(" | <a href=\"index.php?commande=$commande&page=".($page+1)."&start=".($start+50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\">Page suivante</a>");} 
    echo '<br>';
	echo '<br>';
	
	?>
                      <?php } else { 
					  
	@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;} 

	$commande = "SELECT * FROM parc ORDER BY station";

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
	
	$select = $commande." LIMIT $start,50";
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
// SUITE ...
// si on a récupéré un résultat on l'affiche.
if($total) {
    // début du tableau
    echo '<table width="620" class="ligne_box" border="0" cellspacing="0" cellpadding="0">'."\n";
        // première ligne on affiche les titres
        echo '<tr>';
        echo '<td width="90" bgcolor="#666666"><b><font color="#FFFFFF">NOM [STATION]</font></b></td>';
		echo '<td width="60" bgcolor="#666666"><b><font color="#FFFFFF">MARQUE</font></b></td>';
		echo '<td width="90" bgcolor="#666666"><b><font color="#FFFFFF">MODELE</font></b></td>';
		echo '<td width="80" bgcolor="#666666"><b><font color="#FFFFFF">N&deg; DE SERIE</font></b></td>';
        echo '<td width="75" bgcolor="#666666"><b><font color="#FFFFFF">ADRESSE IP</font></b></td>';
		echo '<td width="80" align="center" bgcolor="#666666"><b><font color="#FFFFFF">ANTIVIRUS</font></b></td>';
		echo '<td width="145" bgcolor="#666666"><b><font color="#FFFFFF">UTILISATEUR [LOGIN]</font></b></td>';
        echo '</tr>'."\n";
    // lecture et affichage des résultats, 1 résultat par ligne.    
    while($row = mysqli_fetch_array($result)) {
        echo '<tr onMouseOver="style.backgroundColor=\'#FFFFFF\'" onMouseOut="style.backgroundColor=\'\'" style="border-bottom: 1px solid #CACACA">';
        echo '<td class="padding2" width="90" height="30" style="border-bottom: 1px solid #CACACA"><font size="1"><b>'.("<a onclick=\"MM_openBrWindow('fichepc.php?infos=sessions&commande=".($row['station'])."', 'FICHEPC', 'width=600,height=550,top=50,left=50')\" STYLE=\"text-decoration: none;color: blue;cursor:pointer;\">").$row['station'].'</a></b></font></td>';
		echo '<td width="60" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['marque'].'</font></td>';
		echo '<td width="90" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['modele'].'</font></td>';
		echo '<td width="80" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['serial'].'</font></td>';
        echo '<td width="75" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['ip'].'</font></td>';
		if ($row['antivirus']=="") {echo '<td width="80" height="30" align="center" style="border-bottom: 1px solid #CACACA"><img src="Icones/checkR.png" alt=""/></td>';}
		else {echo '<td width="80" height="30" align="center" style="border-bottom: 1px solid #CACACA"><img src="Icones/checkV.png" alt=""/></td>';}
		echo '<td width="145" height="30" style="border-bottom: 1px solid #CACACA"><font color="#000000">'.$row['login'].'</font></td>';
        echo '</tr>'."\n";
    }
        echo '<tr>';
        echo '<td width="90">&nbsp;</td>';
		echo '<td width="60">&nbsp;</td>';
		echo '<td width="90">&nbsp;</td>';
		echo '<td width="80">&nbsp;</td>';
        echo '<td width="75">&nbsp;</td>';
		echo '<td width="80">&nbsp;</td>';
		echo '<td width="145">&nbsp;</td>';
        echo '</tr>'."\n";
    echo '</table>'."\n";
    // fin du tableau.
}
else echo '<font color="#3366CC"><b>Aucun enregistrement ne correspond à votre requête...</b></font>';

// on libère le résultat
mysqli_free_result($result);

@$start = $_REQUEST['start'];
	if ($start=="")
	{@$start = $_GET['start'];}
	
	if(!$start) {$start=0;} 

	@$commande= $_REQUEST['commande']; // On récupere la commande sql passée soit par un formulaire soit par l'adresse
    if($commande=="")
    {@$commande= $_GET['commande'];}
	
	if(!$commande) {$commande ="SELECT * FROM parc ORDER BY station";}

    $select = $commande;
	$result = $connection->query ($select);
    $total = mysqli_num_rows($result);
	
	@$page = $_REQUEST['page'];
	if (!$page)
	{@$page=1;}
	
	//Affichage "Page précèdente" 
    if($start>0){ 
    echo("<a href=\"index.php?commande=$commande&page=".($page-1)."&start=".($start-50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\">Page pr&eacute;c&eacute;dente</a>");}
    //Affichage "Page suivante" 
    if($total>$start+50){ 
    echo(" | <a href=\"index.php?commande=$commande&page=".($page+1)."&start=".($start+50)."&checkp=".($checkp)."&checkl=".($checkl)."&checku=".($checku)."&checkv=".($checkv)."\">Page suivante</a>");} 
    echo '<br>';
	echo '<br>';					  				  
					  }
					  ?>
                    </div>
              </div>
                <div class="paragraphe"> </div>
            </div>

        </div>
        <div class="menu_v">
            <div class="menu_v_ctn">
	            <span class="menu_v_titre">Apercu rapide :</span>
                <a href="#"><br>
              Connexions du jour :</a><br>
                <canvas id="canvas1" height="120" width="120"></canvas>
                <script>
	var color1 = '#22B14C';
	var color2 = '#F79398';
		var pieData = [
		<?php
		$date = date("Y-m-d");
		$resultdatetoday = $connection->query ("SELECT date FROM parc WHERE date='$date' ORDER BY date");
		$countdatetoday = mysqli_num_rows($resultdatetoday);	
		$resultdateafter = $connection->query ("SELECT date FROM parc WHERE date<'$date' ORDER BY date");
		$countdateafter = mysqli_num_rows($resultdateafter);
				echo '{';
					echo 'value: '.$countdatetoday.',';
					echo 'color: color1';
				echo '},';
				
				echo '{';
					echo 'value: '.$countdateafter.',';
					echo 'color: color2';
				echo '},';		
		?>
			];

	var myPie = new Chart(document.getElementById("canvas1").getContext("2d")).Pie(pieData);	
	            </script>
                <br>
                <?php
		$color1 = '#22B14C';
	    $color2 = '#F79398';
		$date = date("Y-m-d");
		$resultdatetoday = $connection->query ("SELECT date FROM parc WHERE date='$date' ORDER BY date");
		$countdatetoday = mysqli_num_rows($resultdatetoday);	
		$resultdateafter = $connection->query ("SELECT date FROM parc WHERE date<'$date' ORDER BY date");
		$countdateafter = mysqli_num_rows($resultdateafter);
		echo '<table width="100%" border="0">'	."\n";
		    echo '<tr>';
			echo '<td width="18" height="30" align="center" valign="middle" class="text11px" bgcolor='.$color1.'>'.$countdatetoday.'</td>';
			echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE date = \''.$date.'\' ORDER BY station">PC connect&eacute;s aujourd\'hui</a></td>';
			echo '</tr>';
		    echo '<tr>';
			echo '<td width="18" height="30" align="center" valign="middle" class="text11px" bgcolor='.$color2.'>'.$countdateafter.'</td>';
			echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE date < \''.$date.'\' ORDER BY station">PC "NON" connect&eacute;s</a></td>';
			echo '</tr>';
		echo '</table>'	."\n";
		?>
<br>
                <a href="#">Syst&egrave;mes [OS] :</a><br>
		<canvas id="canvas2" height="120" width="120"></canvas>
        
	<script>
	var color1 = '#69D2E7';
	var color2 = '#0080FF';
	var color3 = '#003C6E';
	var color4 = '#E0E4CC';
		var pieData = [
		<?php
		$n = 1;
		$resultos = $connection->query ("SELECT DISTINCT os FROM parc ORDER BY os");
		
		while ($row_os = mysqli_fetch_assoc($resultos)) {
			
			$sqlcountos = $connection->query ("SELECT os FROM parc WHERE os='".$row_os['os']."'");
			$countos = mysqli_num_rows($sqlcountos);
			
				echo '{';
					echo 'value: '.$countos.',';
					echo 'color: color'.$n.'';
				echo '},';
			$n = $n +1;
		}
		?>
			];

	var myPie = new Chart(document.getElementById("canvas2").getContext("2d")).Pie(pieData);	
	</script>
	<br>
        <?php
		$color1 = '#69D2E7';
	    $color2 = '#0080FF';
	    $color3 = '#003C6E';
		$color4 = '#E0E4CC';
		$m = 1;
		
		$resultos = $connection->query ("SELECT DISTINCT os FROM parc ORDER BY os");
		echo '<table width="100%" border="0">'	."\n";
		while ($row_os = mysqli_fetch_assoc($resultos)) {
			$sqlcountos = $connection->query ("SELECT os FROM parc WHERE os='".$row_os['os']."'");
			$countos = mysqli_num_rows($sqlcountos);
            if ($m==2) {
				$color = $color2;
			} else {
				if ($m==3) {
					$color = $color3;
				} else {
					if ($m==1) {
						$color = $color1;
					}else {$color = $color4;}
				}
			}
		    echo '<tr>';
			echo '<td width="18" height="30" align="center" valign="middle" class="text11px" bgcolor='.$color.'>'.$countos.'</td>';
			echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE os = \''.$row_os['os'].'\' ORDER BY station">'.$row_os['os'].'</a></td>';
			echo '</tr>';
			$m = $m + 1;
		} ;
		echo '</table>'	."\n";
		?>
                <br />
                <a href="#">Suite Ms Office :</a><br>
		<canvas id="canvas3" height="120" width="120"></canvas>
        
	<script>
	var color1 = '#20A447';
	var color2 = '#28D25B';
	var color3 = '#9BFF9B';
	var color4 = '#FFFFFF';
		var pieData = [
		<?php
		$n = 1;
		$resultoffice = $connection->query ("SELECT DISTINCT office FROM parc ORDER BY office DESC");
		
		while ($row_office = mysqli_fetch_assoc($resultoffice)) {
			
			$sqlcountoffice = $connection->query ("SELECT office FROM parc WHERE office='".$row_office['office']."'");
			$countoffice = mysqli_num_rows($sqlcountoffice);
			
				echo '{';
					echo 'value: '.$countoffice.',';
					echo 'color: color'.$n.'';
				echo '},';
			$n = $n +1;
		}
		?>
			];

	var myPie = new Chart(document.getElementById("canvas3").getContext("2d")).Pie(pieData);	
	</script>
	<br>
        <?php
		$color1 = '#20A447';
	    $color2 = '#28D25B';
	    $color3 = '#9BFF9B';
		$color4 = '#FFFFFF';
		$m = 1;
		
		$resultoffice = $connection->query ("SELECT DISTINCT office FROM parc ORDER BY office DESC");
		echo '<table width="100%" border="0">'	."\n";
		while ($row_office = mysqli_fetch_assoc($resultoffice)) {
			$sqlcountoffice = $connection->query ("SELECT office FROM parc WHERE office='".$row_office['office']."'");
			$countoffice = mysqli_num_rows($sqlcountoffice);
			if ($m==1) {$color = $color1;}
			if ($m==2) {$color = $color2;}
			if ($m==3) {$color = $color3;}
			if ($m==4) {$color = $color4;}
		    echo '<tr>';
			echo '<td width="18" height="30" align="center" valign="middle" class="text11px" bgcolor='.$color.'>'.$countoffice.'</td>';
			if ($row_office['office'] == "")
			{echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE office = \''.$row_office['office'].'\' ORDER BY station">Aucune Suite Ms Office</a></td>';}
			else
			{echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE office = \''.$row_office['office'].'\' ORDER BY station">'.$row_office['office'].'</a></td>';}
			echo '</tr>';
			$m = $m + 1;
		} ;
		echo '</table>'	."\n";
		?>
        <br>
        <a href="#">Mod&egrave;les [PC] :</a><br>
        <canvas id="canvas4" height="120" width="120"></canvas>
        <script>
	var color1 = '#F38630';
	var color2 = '#EFE30A';
	var color3 = '#804000';
	var color4 = '#800000';
	var color5 = '#808000';
	var color6 = '#EFE4B0';
	var color7 = '#FFFFFF';
		var pieData = [
		<?php
		$n = 1;
		$resultmodele = $connection->query ("SELECT DISTINCT modele FROM parc ORDER BY modele");
		
		while ($row_modele = mysqli_fetch_assoc($resultmodele)) {
			
			$sqlcountmodele = $connection->query ("SELECT modele FROM parc WHERE modele='".$row_modele['modele']."'");
			$countmodele = mysqli_num_rows($sqlcountmodele);
			
				echo '{';
					echo 'value: '.$countmodele.',';
					echo 'color: color'.$n.'';
				echo '},';
			$n = $n +1;
		}
		?>
			];

	var myPie = new Chart(document.getElementById("canvas4").getContext("2d")).Pie(pieData);	
	    </script>
        <br>
        <?php
		$color1 = '#F38630';
	    $color2 = '#EFE30A';
	    $color3 = '#804000';
		$color4 = '#800000';
		$color5 = '#808000';
		$color6 = '#EFE4B0';
		$color7 = '#FFFFFF';
		$m = 1;
		
		$resultmodele = $connection->query ("SELECT DISTINCT marque, modele FROM parc ORDER BY modele");
		echo '<table width="100%" border="0">'	."\n";
		while ($row_modele = mysqli_fetch_assoc($resultmodele)) {
			$sqlcountmodele = $connection->query ("SELECT marque, modele FROM parc WHERE modele='".$row_modele['modele']."'");
			$countmodele = mysqli_num_rows($sqlcountmodele);
			if ($m==1) {$color = $color1;}
			if ($m==2) {$color = $color2;}
			if ($m==3) {$color = $color3;}
			if ($m==4) {$color = $color4;}
			if ($m==5) {$color = $color5;}
			if ($m==6) {$color = $color6;}
			if ($m==7) {$color = $color7;}
		    echo '<tr>';
			echo '<td width="18" height="30" align="center" valign="middle" class="text11px" bgcolor='.$color.'>'.$countmodele.'</td>';
			echo '<td class="text11pxwhite"><a href="index.php?commande=SELECT * FROM parc WHERE modele = \''.$row_modele['modele'].'\' ORDER BY station">'.$row_modele['marque'].' '.$row_modele['modele'].'</a></td>';
			echo '</tr>';
			$m = $m + 1;
		} ;
		echo '</table>'	."\n";
		?>
<br>
              <br>
            </div>
            <div class="menu_v_ctn">
	            <span class="menu_v_titre">Extractions excel :</span>
                <a href="#">A venir bientot...</a><br />
                <a href="#">A venir bientot...</a>
            </div>
        </div>
        <div class="clean"></div>
        <div class="foot">
            <span>
				                <a href="http://validator.w3.org/check?uri=referer">Copyright 2004-2015 by Mister Fox</a><a href="http://www.supportduweb.com/"></a>
            </span>
        </div>
    </div>
    </div>
</body>
</html>
