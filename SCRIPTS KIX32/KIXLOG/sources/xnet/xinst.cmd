@echo off
rem
rem  XINST.CMD
rem
rem  Sample batchfile demonstrating how the KiXtart RPC service can be
rem  installed on a remote system.
rem
rem  Note :  This code sample is provided for demonstration purposes only.
rem          Microsoft makes no warranty, either express or implied,
rem          as to its usability in any given situation.
rem
IF "%1" == "" GOTO ERR0
ECHO Please read the installation instructions of KXRPC before running this batch.
ECHO Press Enter to install KXRPC on %1...
MD \\%1\C$\KIXTART
IF ERRORLEVEL 1 GOTO ERR1
COPY KXRPC.EXE \\%1\C$\KIXTART
IF ERRORLEVEL 1 GOTO ERR2
XNET INSTALL \\%1\KXRPC /b:C:\KIXTART\KXRPC.EXE /n:"KiXtart RPC Service" /d:RPCSS /s:AUTO
ECHO KXRPC service successfully installed on %1!
GOTO END

:ERR0
ECHO XINST   : Batchfile to install KiXtart RPC service on a remote server.
ECHO Usage   : XINST (name of logon server to install KXRPC on)
ECHO Example : XINST MYSERVER
GOTO END

:ERR1
ECHO Failed to create directory on %1!
GOTO END

:ERR2
ECHO Failed to copy KXRPC.EXE to directory C:\KIXTART on %1!
GOTO END

:END
