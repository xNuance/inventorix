<?php require_once('connexion.php'); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>INFORMATIX - Effectuer une recherche sur la base</title>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
.Style2 {color: #FFFFFF}
.Style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style></head>

<body background="Icones/fondbleu.jpg">
<form method="post" action="search.php">
  <table width="793" border="1" align="center" cellspacing="0">
    <tr>
      <td width="57" height="44" bgcolor="#FFFFFF">
<div align="center"><img src="Icones/search.gif" width="48" height="48"> </div>
      <div align="center"></div></td>
    <td width="726" nowrap background="Icones/banner.gif"><div align="center" class="Style2">Recherche</div></td>
  </tr>
    <tr bgcolor="#CCCCCC"> 
      <td colspan="2"> 
        <div align="left">
      <blockquote>
        <p align="left"><center><font color=#FF0000><?php echo @$erreur; ?></font></center>
              <span class="Style6"><br>
            Votre recherche concerne la table :          
            <label>
    <input name="choixbase" type="radio" value="archive">
    Archive</label>
              <label>
              <input name="choixbase" type="radio" value="actuelle" checked>
    Actuelle</label>
              <br>
            </span></p>
        <p class="Style6"><u>Champs de recherche :</u></p>
      </blockquote>
      <div align="center" class="Style6"></div>
      <center>
	  <table width="510" border="1">
        <tr>
          <td width="82" class="Style6">Date : </td>
          <td width="144" class="Style6"><input name="date" type="text" id="date"></td>
          <td width="112" class="Style6">Heure : </td>
          <td width="144" class="Style6"><input name="heure" type="text" id="heure"></td>
        </tr>
        <tr>
          <td class="Style6">Login : </td>
          <td class="Style6"><input name="login" type="text" id="login"></td>
          <td class="Style6">Nom : </td>
          <td class="Style6"><input name="utilisateur" type="text" id="utilisateur"></td>
        </tr>
        <tr>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
        </tr>
        <tr>
          <td height="26" class="Style6">Station : </td>
          <td class="Style6"><input name="station" type="text" id="station"></td>
          <td class="Style6">IP : </td>
          <td class="Style6"><input name="ip" type="text" id="ip"></td>
        </tr>
        <tr>
          <td class="Style6">MAC : </td>
          <td class="Style6"><input name="mac" type="text" id="mac"></td>
          <td class="Style6">Num&eacute;ro s&eacute;rie : </td>
          <td class="Style6"><input name="numeroserie" type="text" id="numeroserie"></td>
        </tr>
        <tr>
          <td class="Style6">Processeur : </td>
          <td class="Style6"><input name="processeur" type="text" id="processeur"></td>
          <td class="Style6">Imprimante : </td>
          <td class="Style6"><input name="imprimante" type="text" id="imprimante"></td>
        </tr>
        <tr>
          <td class="Style6">Puissance : </td>
          <td colspan="2" class="Style6"><p align="center"><label><input type="radio" name="optvitesse" value="sup">
      Superieure</label>
              <label>
              <input name="optvitesse" type="radio" value="egal" checked>
      Egale</label>
              <label>
              <input type="radio" name="optvitesse" value="inf">
      Inferieure</label>
          </p></td>
          <td class="Style6"><input name="vitesse" type="text" id="vitesse"></td>
        </tr>
        <tr>
          <td class="Style6">Memoire : </td>
          <td colspan="2" class="Style6"><p align="center">
              <label>
              <input type="radio" name="optmemoire" value="sup">
      Superieure</label>
              <label>
              <input name="optmemoire" type="radio" value="egal" checked>
      Egale</label>
              <label>
              <input type="radio" name="optmemoire" value="inf">
      Inferieure</label>
          </p></td>
          <td class="Style6"><input name="memoire" type="text" id="memoire"></td>
        </tr>
        <tr>
          <td class="Style6">Disque : </td>
          <td colspan="2" class="Style6"><p align="center">
                    <label><input type="radio" name="optdisque" value="sup">
            Superieur</label>
                    <label>
                    <input name="optdisque" type="radio" value="egal" checked>
            Egal</label>
                    <label>
                    <input type="radio" name="optdisque" value="inf">
            Inferieur</label>
          </p></td>
          <td class="Style6"><input name="disque" type="text" id="disque"></td>
        </tr>
        <tr>
          <td class="Style6">OS : </td>
          <td class="Style6"><input name="os" type="text" id="os"></td>
          <td class="Style6">Service Pack : </td>
          <td class="Style6"><input name="sp" type="text" id="sp"></td>
        </tr>
        <tr>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
        </tr>
        <tr>
          <td class="Style6">Antivirus : </td>
          <td class="Style6"><input name="antivirus" type="text" id="antivirus"></td>
          <td class="Style6">Cle AVP : </td>
          <td class="Style6"><input name="cleavp" type="text" id="cleavp"></td>
        </tr>
        <tr>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
        </tr>
        <tr>
          <td class="Style6">Internet : </td>
          <td class="Style6"><input name="internet" type="text" id="internet"></td>
          <td class="Style6">Version IE : </td>
          <td class="Style6"><input name="ieversion" type="text" id="ieversion"></td>
        </tr>
        <tr>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
        </tr>
        <tr>
          <td class="Style6">C-Page : </td>
          <td class="Style6"><input name="cpage" type="text" id="cpage"></td>
          <td class="Style6">Patch C-Page : </td>
          <td class="Style6"><input name="patchcpage" type="text" id="patchcpage"></td>
        </tr>
        <tr>
          <td class="Style6">Crossway : </td>
          <td class="Style6"><input name="crossway" type="text" id="crossway"></td>
          <td class="Style6">Winrest : </td>
          <td class="Style6"><input name="winrest" type="text" id="winrest"></td>
        </tr>
        <tr>
          <td class="Style6">Lotus : </td>
          <td class="Style6"><input name="lotus" type="text" id="lotus"></td>
          <td class="Style6">VNC : </td>
          <td class="Style6"><input name="vnc" type="text" id="vnc"></td>
        </tr>
        <tr>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
          <td class="Style6">&nbsp;</td>
        </tr>
        <tr>
          <td class="Style6">Word : </td>
          <td class="Style6"><input name="word" type="text" id="word"></td>
          <td class="Style6">Excel : </td>
          <td class="Style6"><input name="excel" type="text" id="excel"></td>
        </tr>
        <tr>
          <td class="Style6">Powerpoint : </td>
          <td class="Style6"><input name="powerpoint" type="text" id="powerpoint"></td>
          <td class="Style6">Access : </td>
          <td class="Style6"><input name="access" type="text" id="access"></td>
        </tr>
      </table>
	  <br>
	  <input type="submit" name="Submit" value="Rechercher !">
      </center>
      <p align="center"> <br>
            <br>
          </p>
    </div></td>
    </tr>
</table>
</form>
</body>
</html>
